<?php
try {
    //$pdo = new PDO('mysql:host=localhost;dbname=symfonyyt', 'root', '');
    //$videoTitle = 'culpa';
    //$query = 'SELECT * FROM video WHERE name = :title_video';
    //$resultat = $pdo->prepare($query);
    //$resultat->execute([
    //    ':title_video' => $videoTitle
    //]);
    //$rows = $resultat->fetchAll(PDO::FETCH_ASSOC);
    //dump($rows);


//    $genre = "naruto";
//    $query = "INSERT INTO genre(name) VALUES (:name_genre)";$resultat = $pdo->prepare($query);
//    $resultat->execute([
//        ':name_genre' => $genre
//    ]);
    $pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');
    if (isset($_POST['title_video'])) {
        $title = '%' . $_POST['title_video'] . '%';
        $query = "SELECT * FROM movie WHERE title LIKE :title_video ";
        $resultat = $pdo->prepare($query);
        $resultat->execute([
            ':title_video' => $title
        ]);
    } else {
        $query = 'SELECT * FROM movie ';
        $resultat = $pdo->prepare($query);
        $resultat->execute([
        ]);
    }
    $rows = $resultat->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    var_dump($e);
}
?>


    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exam css</title>
        <link rel="stylesheet" type="text/css" href="/assets/index.css"/>
    </head>

    <body>
    <div class="container">

        <form action="index.php" method="post">
            <p>Titre du film : <input type="text" name="title_video"/></p>
            <p><input type="submit" value="OK"></p>
        </form>

        <div class="containerCard">
            <?php
            foreach ($rows as $row) {
                ?>
                <div class="card">
                    <div>
                        <p><?php echo $row['title'] ?></p>
                    </div>
                    <div class="cardImgBackground"><img src="<?php echo $row['image_link'] ?>"></div>
                    <div>
                        <p>
                        </p>
                        <div class="cardButton">
                            <a href="movie.php?id=<?php echo $row['id']; ?>">
                                <button>Voir film</button>
                            </a>
                            <a href="treatDeleteMovie.php?id=<?php echo $row['id']; ?>">
                                <button>Supprimer le film</button>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <a href="addMovie.php">
            <button>Ajouter un film</button>
        </a>
    </div>

    </body>

    </html>

<?php

function dump($value)
{
    echo '<pre>';
    var_dump($value);
    echo '</pre>';
}

//function html($value){
/*    echo '<?php $value ?>';*/
//}
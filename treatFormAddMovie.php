<?php

try {
    $title_video = $_POST['title_video'];
    $launch_at = $_POST['launch_at'];
    $image_link = $_POST['image_link'];
    $genre_id = $_POST['genre_id'];
    $pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');

    $videoTitle = '';
    $query = 'INSERT INTO movie (title, launch_at , image_link, genre_id )
    VALUES (:title_video, :launch_at, :image_link, :genre_id)
';
    $resultat = $pdo->prepare($query);
    $resultat->execute([
        ':title_video' => $title_video,
        ':launch_at' => $launch_at,
        ':image_link' => $image_link,
        ':genre_id' => $genre_id
    ]);
    header('Location: index.php');


} catch (Exception $e) {
    dump($e);
    var_dump($e);
}

function dump($value)
{
    echo '<pre>';
    var_dump($value);
    echo '</pre>';
}
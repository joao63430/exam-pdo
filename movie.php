<?php

try {
    $id = $_GET['id'];
    $pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');

    $query = 'SELECT * FROM movie
INNER JOIN genre ON movie.genre_id = genre.id
WHERE movie.id = :id_video';
    $resultat = $pdo->prepare($query);
    $resultat->execute([
       ':id_video' => $id
    ]);
    $row = $resultat->fetch(PDO::FETCH_ASSOC);

} catch (Exception $e) {
    var_dump($e);
}

?>

<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exam css</title>
        <link rel="stylesheet" type="text/css" href="/assets/index.css"/>
    </head>

    <body>
    <div class="container">

    <div class="containerCard">
        <?php
            ?>
            <div class="card">
                <div>
                    <h1 style="text-align: center"><?php echo $row['title'] ?></h1>
                </div>
                <div class="cardImgBackground"><img src="<?php echo $row['image_link'] ?>"></div>
                <div>
                    <p>
                        Genre : <?php echo $row['label'] ?>
                    </p>
                    <p> date de sortie :
                        <?php
                        $date =new DateTime($row['launch_at']);
                        echo $date->format('Y-m-d '); ?>
                    </p>
                    <div class="cardButton">
                        <a href="index.php"><button>retour à l'accueil</button></a>
                    </div>
                </div>
            </div>
    </div>
    </div>

    </body>

    </html>


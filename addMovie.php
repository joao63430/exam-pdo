<?php
try {

$pdo = new PDO('mysql:host=localhost;dbname=exam', 'root', '');

$videoTitle = '';
$query = 'SELECT * FROM genre ';
$resultat = $pdo->prepare($query);
$resultat->execute([
]);
$rows = $resultat->fetchAll(PDO::FETCH_ASSOC);

} catch (Exception $e) {
var_dump($e);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam css</title>
    <link rel="stylesheet" type="text/css" href="/assets/index.css"/>
</head>

<body>
<div class="container">
    <form action="treatFormAddMovie.php" method="post">
        <p>Titre du film : <input type="text" name="title_video" /></p>
        <p>Lien url pour image : <input type="url" name="image_link" /></p>
        <p>Selectionner la date de sortie : <input type="date" name="launch_at" /></p>
        <p>Selectionner le genre :
            <select name="genre_id" id="color">
                <option value="">--- Choisir un genre ---</option>
                <?php
                foreach ($rows as $row) {
                ?>
                    <option value="<?php echo $row['id'] ?>"><?php echo $row['label'] ?></option>
        <?php } ?>
            </select>
        </p>

        <p><input type="submit" value="OK"></p>
    </form>
</div>

</body>

</html>

